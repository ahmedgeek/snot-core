var Client = require('./client.js');
var kafkaManager = require('./kafkaManager');

class Pool{
    constructor(){
        this._pool = [];
        this._kill_interval = 60000;
        this.keep_clean();
        this.kafkaManager = new kafkaManager();
    }

    push(pckt) {
        let client = this.locate(pckt.mac2);
        if( client == undefined) {
            var client_by_seq = this.locate_by_seq(pckt.seq);
            if(client_by_seq !== undefined) {
                client_by_seq.attach_child(pckt.mac2);
                client_by_seq.probed();
                client_by_seq.seq = pckt.seq;
            } else {
                this._pool.push(new Client(pckt.mac2, pckt.rssi, pckt.seq));
                this.kafkaManager.send('snot_probe', JSON.stringify(pckt));
            }
        } else {
            client.probed();
            client.seq = pckt.seq;
            this.kafkaManager.send('snot_probe_update', JSON.stringify(pckt));
        }
    }

    locate(mac) {
        return this._pool.find(o => o.mac === mac);
    }

    locate_by_seq(seq) {
        return this._pool.find(o => (seq - o.seq) >= 1 && (seq - o.seq) <= 15000);
    }

    drown(mac) {
        let obj = this.locate(mac);
        delete this._pool[this._pool.indexOf(obj)];
    }

    net_all() {
        //sorted by sequence number
        return this._pool.sort((a, b) => (a.seq > b.seq) ? 1 : -1);
    }

    keep_clean() {
        var _that = this;
        setInterval(function(){
            for(var client in _that._pool) {
                if(_that._pool[client].last_probe_diff() >= _that._kill_interval) {
                    _that._pool.splice(client, 1);
                }
            }
        }, 5000);
    }
}

module.exports = Pool;