class Client{
    constructor(mac, rssi, seq) {
        this.mac = mac;
        this.RSSI = rssi;
        this._last_probe = Date.now();
        this._seq = seq;
        this._children = [];
    }

    set mac(value) {
        this._mac = value;
    }

    set RSSI(value) {
        this._RSSI = value;
    }

    set seq(value) {
        this._seq = value;
    }

    get seq() {
        return this._seq;
    }

    get mac() {
        return this._mac;
    }

    get last_probe() {
        return this._last_probe;
    }

    get RSSI() {
        return this._RSSI;
    }

    get children() {
        return this._children;
    }

    probed() {
        this._last_probe = Date.now();
    }

    attach_child(address) {
        this._children.push(address);
    }

    last_probe_diff() {
        return Date.now() - this._last_probe;
    }

    last_probe_string() {
        return Date(this._last_probe).toString();
    }
}

module.exports = Client;