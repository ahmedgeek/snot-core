class kafkaManager {
    constructor() {
        var config = require('config');
        const {Kafka} = require('kafkajs')
        this._kafkaClient = new Kafka({
            clientId: 'my-app',
            brokers: config.get('brokers'),
            ssl: {
                rejectUnauthorized: true
            },
            sasl: {
                mechanism: 'plain',
                username: config.get('kafkaUsername'),
                password: config.get('kafkaPassword'),
            }
        });

        this._kafkaProducer = this._kafkaClient.producer();
    }

    async connect() {
        await this._kafkaProducer.connect();
    }

    async send(topic, payload) {
        await this._kafkaProducer.send({
            topic: topic,
            messages: [{
                value: payload
            }],
        });
    }
}

module.exports = kafkaManager;