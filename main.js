process.env["NODE_CONFIG_DIR"] = __dirname + "/config/";


const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const config = require('config');
const ClientsPool = require('./lib/pool.js');
const nodeManager = require('./lib/nodeManager.js');

const Pool = new ClientsPool();
const Nodes = new nodeManager();

http.listen(config.get('socketPort'), function () {
    console.log('listening on *:3000');
});

io.on('connection', function (socket) {

    if (socket.handshake.query.hasOwnProperty('token') && socket.handshake.query.hasOwnProperty('snot_id')) {
        socket.auth = Nodes.authenticate(socket.handshake.query.snot_id ,socket.handshake.query.token);
    } else {
        socket.auth = false;
        socket.close();
    }
    socket.auth = false;
}).on('authenticated', function(){
    console.log('authed');
    socket.on('message', async function (msg) {
        Pool.push(msg);
    });
});